from PIL import Image
from typing import Tuple, List, Mapping, NoReturn

from enum import Enum

class TokenType(Enum):
    ADD = 'Add'
    SUB = 'Subract'
    MUL = 'Multiply'
    DIV = 'Divide'
    NUMBER = 'Load number to temporary memory'
    BRANCH_IF_ZERO = 'Branch if zero'
    BRANCH_IF_GT_ZERO = 'Branch if greater than zero'
    BRANCH_IF_LT_ZERO = 'Branch if less than zero'
    LOAD_FROM_MEMORY = 'Load number from memory'
    LOAD_TO_MEMORY = 'Load number to memory'
    LOAD_N_TO_MEMORY = 'Load n pixels to memory'
    
    PRINT_VALUE = 'Print value'
    PRINT_ASCII = 'Print ascii'
    
    INVALID = 'Invalid'

from dataclasses import dataclass

@dataclass
class Token:
    filename: str
    pixel: Tuple[int, int]
    values: Tuple[int, int, int]
    token_type: TokenType
    image_size: Tuple[int, int]

class Parse:
    def load_tokens(self, filename: str) -> List[Token]:
        im = Image.open(filename, 'r')
        width, height = im.size
        tokens: List[Token] = []

        for i, values in enumerate(im.getdata()):
            op, _, _, *rest = values
            if op == 0:
                continue
            
            token_type: TokenType = TokenType.INVALID
            
            type_map: Mapping[int, TokenType] = {
                10: TokenType.NUMBER,
                20: TokenType.ADD,
                30: TokenType.SUB,
                40: TokenType.MUL,
                50: TokenType.DIV,
                60: TokenType.BRANCH_IF_ZERO,
                70: TokenType.BRANCH_IF_GT_ZERO,
                80: TokenType.BRANCH_IF_LT_ZERO,
                90: TokenType.LOAD_FROM_MEMORY,
                100: TokenType.LOAD_TO_MEMORY,
                110: TokenType.LOAD_N_TO_MEMORY,
                120: TokenType.PRINT_ASCII,
                130: TokenType.PRINT_VALUE,
            }
            
            for value in sorted(type_map.keys(), reverse=True):
                if value <= op:
                    token_type = type_map[value]
                    break
            
            tokens.append(Token(filename, (i % width, i // width), values[:3], token_type, (width, height)))
        return tokens

class Variables:
    def __init__(self, length: int) -> None:
        self.memory: List[int] = [0] * length
    
    def size(self) -> int:
        return len(self.memory)
    
    def get(self, index: int) -> int:
        if index < 0 or index > len(self.memory):
            index = 0
        # print(f"Memory get [ address: {index}  value: {self.memory[index]} ]")
        return self.memory[index]

    def set(self, index: int, value: int) -> None:
        if index < 0 or index > self.size():
            index = 0
        # print(f"Memory set [ address: {index}  value: {value} ]")
        self.memory[index] = value

class Run:
    VARIABLES: Variables = Variables(256)
    HEAP: Variables = Variables(10000)
    
    def error(self, token: Token, message: str) -> NoReturn:
        im = Image.open(token.filename, 'r')
        pix = im.load()
        pix[token.pixel] = (255 - token.values[0], 255 - token.values[1], 255 - token.values[2])
        im.show()
        raise Exception(f"{token.filename}:{token.pixel[0]}:{token.pixel[1]} {token.values} {token.token_type.value}: {message}")

    def run(self, tokens: List[Token]) -> None:
        index: int = 0
        while index < len(tokens):
            token: Token = tokens[index]
            token_type: TokenType = token.token_type
            
            if token_type == TokenType.NUMBER:
                _, n, t = token.values
                self.VARIABLES.set(t, n)
            
            if token_type == TokenType.LOAD_TO_MEMORY:
                _, n, t = token.values
                memory_address = self.VARIABLES.get(t)
                if memory_address < 0 or memory_address >= self.HEAP.size():
                    self.error(token, f"Unable to load a number to memory address {memory_address}. It is out of bounds of heap size: {self.HEAP.size()}")
                self.HEAP.set(memory_address, self.VARIABLES.get(n))
            
            if token_type == TokenType.LOAD_FROM_MEMORY:
                _, n, t = token.values
                memory_address = self.VARIABLES.get(n)
                if memory_address < 0 or memory_address >= self.HEAP.size():
                    self.error(token, f"Unable to load a number from memory address {memory_address}. It is out of bounds of heap size: {self.HEAP.size()}")
                self.VARIABLES.set(t, self.HEAP.get(memory_address))
            
            if token_type == TokenType.LOAD_N_TO_MEMORY:
                _, n, address = token.values
                length = self.VARIABLES.get(n)
                memory_address = 0
                copy_index: int = index + 1
                self.VARIABLES.set(address, memory_address)
                while copy_index < len(tokens) and copy_index - index < length:
                    copy_token: Token = tokens[copy_index]
                    data: Tuple[int, int, int] = copy_token.values
                    self.HEAP.set(memory_address + 0, data[0])
                    self.HEAP.set(memory_address + 1, data[1])
                    self.HEAP.set(memory_address + 2, data[0])
                    memory_address += 3
                    if memory_address >= self.HEAP.size():
                        copy_index = index + length + 10
                    copy_index += 1
            
            if token_type == TokenType.ADD:
                _, f, t = token.values
                self.VARIABLES.set(t, self.VARIABLES.get(t) + self.VARIABLES.get(f))
            if token_type == TokenType.SUB:
                _, f, t = token.values
                self.VARIABLES.set(t, self.VARIABLES.get(t) - self.VARIABLES.get(f))
            if token_type == TokenType.MUL:
                _, f, t = token.values
                self.VARIABLES.set(t, self.VARIABLES.get(t) * self.VARIABLES.get(f))
            if token_type == TokenType.DIV:
                _, f, t = token.values
                divisor = self.VARIABLES.get(f)
                if divisor != 0:
                    self.VARIABLES.set(t, self.VARIABLES.get(t) // divisor)
            
            if token_type == TokenType.BRANCH_IF_ZERO:
                _, condition, branch = token.values
                if self.VARIABLES.get(condition) == 0:
                    new_index = self.VARIABLES.get(branch)
                    if new_index < 0 or new_index >= len(tokens):
                        self.error(token, f"Unable to branch to pixel {new_index}. It is out of bounds with image dimensions {token.image_size}, size {token.image_size[0] * token.image_size[1]}")
                    index = new_index
                    continue
            
            if token_type == TokenType.BRANCH_IF_GT_ZERO:
                _, condition, branch = token.values
                if self.VARIABLES.get(condition) > 0:
                    new_index = self.VARIABLES.get(branch)
                    if new_index < 0 or new_index >= len(tokens):
                        self.error(token, f"Unable to branch to pixel {new_index}. It is out of bounds with image dimensions {token.image_size}, size {token.image_size[0] * token.image_size[1]}")
                    index = new_index
                    continue
            
            if token_type == TokenType.BRANCH_IF_LT_ZERO:
                _, condition, branch = token.values
                if self.VARIABLES.get(condition) < 0:
                    new_index = self.VARIABLES.get(branch)
                    if new_index < 0 or new_index >= len(tokens):
                        self.error(token, f"Unable to branch to pixel {new_index}. It is out of bounds with image dimensions {token.image_size}, size {token.image_size[0] * token.image_size[1]}")
                    index = new_index
                    continue
            
            if token_type == TokenType.PRINT_VALUE:
                _, f, _ = token.values
                print(self.VARIABLES.get(f), end='')
            
            if token_type == TokenType.PRINT_ASCII:
                _, f, _ = token.values
                print(chr(self.VARIABLES.get(f)), end='')
            
            index += 1

if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser(
        prog='Image language',
        description='Programming language controlled by images',
    )
    parser.add_argument('filename')
    args = parser.parse_args()
    tokens: List[Token] = Parse().load_tokens(args.filename)
    Run().run(tokens)
