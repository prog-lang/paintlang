# PAINTLANG


### The programming language that you use Paint as an editor


### Usage

```sh
$ python image-lang.py <image>
```

Can use `png`, `jpg`

### Example:
```sh
$ python image-lang.py hello-world.png
Hello World!
```

Where hello-world.png contains rgb values:

(10, 72, 0) : Red is instruction type, in this case it is NUMBER. So it stores 72 in variable 0

(120, 0, *) : PRINT_ASCII of variable 0, blue value is irrelevant so it can be anything, hence the variation in purple.

...


### Usage of this guide

**INSTRUCTION_NAME (RED VALUE RANGE)**

(red_parameter, green_parameter, blue_parameter)

- $ means variable at address
- % means heap at address


---

**NO_OP (0)**

Does nothing

**NUMBER (10 - 20)**

(10-20, value, to)

Stores `value` into variable `to`

```
value -> $to
```



**ADD (20 - 30)**

(20-30, from, to)

Stores variable `from` + variable `to` into variable `to`

```
$from + $to -> $to
```



**SUB (30 - 40)**

(30-40, from, to)

Stores variable `from` - variable `to` into variable `to`

```
$from - $to -> $to
```



**MULTIPLY (40 - 50)**

(40-50, from, to)

Stores variable `from` + variable `to` into variable `to`

```
$from * $to -> $to
```


**DIVIDE (50 - 60)**

(50-60, from, to)

Stores variable `from` - variable `to` into variable `to`. Will not divide if it is dividing by zero.

```
INT($from / $to) -> $to
```


**BRANCH_IF_ZERO (60 - 70)**

(60-70, condition, branch)

Jumps to pixel variable `branch` only if variable `condition` is 0

```
if $condition == 0
    current_pixel = $branch
```


**BRANCH_IF_GT_ZERO (70 - 80)**

(70-80, condition, branch)

Jumps to pixel variable `branch` only if variable `condition` is greater than 0

```
if $condition > 0
    current_pixel = $branch
```


**BRANCH_IF_LT_ZERO (80 - 90)**

(80-90, condition, branch)

Jumps to pixel variable `branch` only if variable `condition` is less than 0

```
if $condition < 0
    current_pixel = $branch
```


**LOAD_FROM_MEMORY (90 - 100)**

(90-100, from, to)

Loads value from heap at address variable `from` and stores it in variable `to`

```
%from -> $to
```


**LOAD_TO_MEMORY (100 - 110)**

(100-110, from, to)

Stores value from variable `from` to heap at address variable `to`

```
$from -> %to
```

**LOAD_N_TO_MEMORY (110 - 120)**

(110-120, n, address)

Stores the next variable `n` pixel values at an address in the heap and stores the start address of the heap data in variable `address`

```
memory_address = malloc(...)
memory_address -> $address
for i in $n:
    next_pixel -> %(memory_address + i)
```

**PRINT_ASCII (120 - 130)**

(120-130, n, *)

Prints the ascii value of the variable `n`

```
print(ASCII($n))
```

**PRINT_VALUE (130 - 140)**

(130-140, n, *)

Prints the value of the variable `n`

```
print($n)
```
